function find(items, cb) {
    if (!!items && !!Array.isArray(items) && !!cb && typeof cb === 'function') {
        for (let i = 0; i < items.length; i++) {
            if(cb(items, items[i])) {
                return items[i];
            }
        }
    }
    else {return [];}
}

module.exports = find;
