function reduce(items, cb, startingValue) {

    if (!!items && !!Array.isArray(items) && !!cb && typeof cb === 'function') {

        if (startingValue === undefined) { startingValue = items[0]; }
        let index = items.indexOf(startingValue)
        for (let itr = index; itr < items.length - 1; itr++) { 
            startingValue = cb(startingValue, items[itr + 1], itr, items); 
        }

        return startingValue;
    }
    return [];
}

module.exports = reduce;
