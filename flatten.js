function nestDigger(arr) {
    if (!!Array.isArray(arr)) {return nestDigger(arr[0]);}
    return arr;
}

function flatten(items) {
    if(!!items && !!Array.isArray(items)) {
        flattenArr = [];
        for (let i = 0; i<items.length; i++) {flattenArr.push(nestDigger(items[i]));}
        return flattenArr;
    }
    return [];
}

module.exports = flatten;
