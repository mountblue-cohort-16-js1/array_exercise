function each(items, cb) {
    if(!!items && !!Array.isArray(items) && !!cb && typeof cb === 'function') {
        for (let itr = 0; itr<items.length; itr++) {
            cb(items, itr);
        }
    }
}

module.exports = each;
