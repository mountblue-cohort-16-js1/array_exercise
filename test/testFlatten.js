const nestedArray = [1, [2], [[3]], [[[4]]]];
const flatten = require('../flatten.js');

console.log(flatten(nestedArray));
