const items = [1, 2, 3, 4, 5, 5];
const reduce = require('../reduce.js');
const cb = function (startingValue, next, itr, items) { return startingValue * next; }

result = reduce(items, cb, 5);

console.log(result);
