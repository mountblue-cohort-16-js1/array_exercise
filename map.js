function map(items, cb) {
    if(!!items && !!Array.isArray(items) && !!cb && typeof cb === 'function') {
        const newArray = [];
        for (let itr = 0; itr<items.length; itr++) {
            newArray.push(cb(items[itr], itr, items));
        }
        return newArray;
    }
    return [];
}

module.exports = map;
