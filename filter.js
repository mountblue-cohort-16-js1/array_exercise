function filter(items, cb) {
    if (!!items && !!Array.isArray(items) && !!cb && typeof cb === 'function') {
        let filteredArr = [];
        for (let i = 0; i < items.length; i++) {
            if(cb(items, items[i])) {filteredArr.push(items[i]);}
        }
        return filteredArr;
    }
    return [];
}

module.exports = filter;
